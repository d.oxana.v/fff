<!DOCTYPE html>

<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="container-fluid">
   
        <div class="h-90 py-2">
          <div class="stylehp">
              <main>
       
      
    </div>
  </main>
</div>
</div>
<div class="container">
    <div class="container-fluid">
        
                         <div class="style">
                <main>
            <h2>form</h2>

            <div id ="messages">
              <?php
                if (!empty($messages)) {
                  foreach ($messages as $message) {
                    print($message);
                    }
                  }
              ?>
              </div>
                <?php
              if (!empty($_SESSION['login']))
              {
                print ('<div class="exit"><a href="logout.php">Exit</a></div>');
              }
              ?> 
            <form action="index.php" method="POST" class="form">
              <ol>
                <li><label> Name<br>
                  <input name="field-name-1" class="<?php if ($errors['field-name-1']) print 'error'?>" value="<?php print $values['field-name-1']?>">
                  </label></li>
                <li><label>Email<br>
                  <input name="field-e-mail" class="<?php if ($errors['field-e-mail']) {print 'error';} ?>" type="email" value="<?php print $values['field-e-mail']; ?>">
                  </label></li>
                <li><label> Date<br>
                  <select name="myselect" value="<?php print $values['myselect']; ?>">
                    <?php
                      $year = 1960;
                      for ($i = 0; $i <= 60; $i++) // Цикл 
                      {
                        $new_years = $year + $i; // Формируем новое значение
                        if ($new_years==$values['myselect']){
                          echo '<option selected value='.$new_years.'>'.$new_years.'</option>'; 
                        }
                        else
                          echo '<option value='.$new_years.'>'.$new_years.'</option>'; 
                      }
                      ?>
                    </select>
                  </label></li>
                <li>Gender<br>
                  <label><input type="radio" <?php if ($values['radio-group-1']=="Женский") print 'checked="checked"'; ?> name="radio-group-1" value="F"> Female</label>
                  <label><input type="radio" <?php if ($values['radio-group-1']=="Мужской") print 'checked="checked"'; ?> name="radio-group-1" value="M"> Male</label>
                  </li>
                <li>Number of limbs
                  <br>
                  <label><input type="radio" <?php if ($values['radio-group-2']=="1") print 'checked="checked"'; ?> name="radio-group-2" value="1">1</label>
                  <label><input type="radio" <?php if ($values['radio-group-2']=="2") print 'checked="checked"'; ?> name="radio-group-2" value="2">2</label>
                  <label><input type="radio" <?php if ($values['radio-group-2']=="4") print 'checked="checked"'; ?> name="radio-group-2" value="4">4</label>
                  <label><input type="radio" <?php if ($values['radio-group-2']=="8") print 'checked="checked"'; ?> name="radio-group-2" value="8">8</label>
                  </li>
                <li>Superpowers <br>
                  <select name="field-name-4[]" multiple="multiple"<?php if ($errors['field-name-4']) {print 'class="error"';} ?>>
                    <option value="Immortality" <?php if (stripos($values['field-name-4'],"Immortality")!==FALSE) print ('selected="selected"'); ?>>Immortality</option>
                    <option value="Passing through walls" <?php if (stripos($values['field-name-4'],"Passing through walls")!==FALSE) print ('selected="selected"'); ?>>Passing through walls</option>
                    <option value="Levitation" <?php if (stripos($values['field-name-4'],"Levitation")!==FALSE) print ('selected="selected"'); ?>>Levitation  </option>
                    <option value="Mind reading" <?php if (stripos($values['field-name-4'],"Mind reading")!==FALSE) print ('selected="selected"'); ?>>Mind reading  </option>
                    <option value="Teleportation" <?php if (stripos($values['field-name-4'],"Teleportation")!==FALSE) print ('selected="selected"'); ?>>Teleportation</option>
                    </select>
                    
                  </li>
                <li><label>Biografia:<br>
                  <textarea name="field-name-2" class="<?php if ($errors['field-name-2']) {print 'error';} ?>"><?php print $values['field-name-2']; ?></textarea>
                  </label></li>
                <li>Accept the privacy policy <br>
                  <label><input type="checkbox" checked="checked" name="check-1">Accept </label>
                  </li>
                </ol>
                <input type="submit" value="Submit">
                
              </form>
              
              <form action="login.php" method="GET">
              <input type="submit" value="enter">
              </form>
            </main>
          </div>
        </div>
    </div>
    </div>
    </div>
    
   
</body>
</html> 
